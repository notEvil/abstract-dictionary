import flask
import jinja2
import lxml.html as l_html
import slob
import copy
import re


application = flask.Flask("__main__")


template = jinja2.Template(
    """
<style>
div.match {
  margin-top: 20pt;
  font-weight: bold;
}

div.type {
  margin-top: 10pt;
  font-style: italic;
}

li.abstract {
  margin-top: 5pt;
}
</style>

<form action="/search">
    <input type="text" name="input" value="{{ input }}" autofocus onFocus="this.select()"></input>
</form>

<ul>
{% for match in matches %}
    <li>
        <div class="match">{{ match['name'] }}</div>
        <div>{{ match['abstract'] }}</div>
    </li>
{% endfor %}
</ul>

<h2>All</h2>
<ul>
{% for match in matches %}
    <li>
        <div class="match">{{ match['name'] }}</div>
        <div>{{ match['content'] }}</div>
    </li>
{% endfor %}
</ul>
"""
)


@application.route("/")
def root():
    global template

    result = template.render()
    return result


@application.route("/search")
def search():
    global template
    global wiktionary_slob

    input_ = flask.request.args["input"]

    # lookup slob

    matches = []

    for _, item in slob.find(input_, [wiktionary_slob]):
        match = dict(name=item.key, content=item.content.decode())
        matches.append(match)

    # reduce to exact matches if available

    exact_matches = [
        match for match in matches if match["name"].lower() == input_.lower()
    ]

    if len(exact_matches) != 0:
        matches = exact_matches

    # get abstract

    for match in matches:
        match["abstract"] = get_abstract(match["content"])

    #

    result = template.render(input=input_, matches=matches)
    return result


def get_abstract(content):
    document = l_html.document_fromstring(content)

    # get toc

    toc_elements = document.xpath(".//*[@id='toc']")

    if len(toc_elements) == 0:
        return ""

    toc_element, = toc_elements

    # parse toc

    def get_toc_node_data(element):
        text_element = element.xpath(".//*[{}]".format(_class_contains("toctext")))[0]
        text = text_element.text

        link_element = element.xpath(".//a")[0]
        link_target = link_element.get("href")
        assert link_target.startswith("#")
        target_id = link_target[1:]

        result = dict(text=text, target_id=target_id)
        return result

    def get_functions(level):
        find_toc_nodes = lambda element: element.xpath(
            ".//*[{}]".format(_class_contains("toclevel-{}".format(level + 1)))
        )

        result = (find_toc_nodes, get_toc_node_data)
        return result

    toc = _parse_toc(toc_element, get_functions)

    # identify targets

    target_ids = {}

    def get_type_nodes(nodes):
        for node in nodes:
            text = node["data"]["text"]

            if re.search(r"^etymology( \d+)?$", text, re.IGNORECASE) is not None:
                yield from get_type_nodes(node["sub_nodes"])
                continue

            if (
                re.search(
                    r"^({})( \d+)?$".format(
                        r"|".join(
                            [
                                "alternative forms",
                                "anagrams",
                                "derived terms",
                                "further reading",
                                "pronunciation",
                                "references",
                                "see also",
                                "statistics",
                                "translations",
                            ]
                        )
                    ),
                    text,
                    re.IGNORECASE,
                )
                is not None
            ):
                continue

            yield node

    for node in toc:
        if node["data"]["text"] != "English":
            continue

        for node in get_type_nodes(node["sub_nodes"]):
            target_ids[node["data"]["target_id"]] = node["data"]["text"]

        break

    # create abstract

    result = []

    for target_id, name in target_ids.items():
        result.extend(['<div class="type">', name, "</div>"])

        target_element, = document.xpath(".//*[@id='{}']".format(target_id))
        list_elements = target_element.xpath("./following::ol")

        if len(list_elements) == 0:
            continue

        result.extend(["<ol>"])

        list_element = list_elements[0]
        item_elements = list_element.xpath("./li")

        for item_element, _ in zip(item_elements, range(5)):
            # remove details
            item_element = _deepcopy_element(item_element)
            _filter_element(
                item_element,
                lambda element: not (
                    isinstance(element, l_html.HtmlElement)
                    and element.tag in ["dl", "ul"]
                ),
            )

            result.extend(
                ['<li class="abstract">', _str_children(item_element), "</li>"]
            )

        if 5 < len(item_elements):
            result.extend(['<li class="abstract">', "...", "</li>"])

        result.extend(["</ol>"])

    result = "\n".join(result)
    return result


# TODO library functions ?


def _parse_toc(element, get_functions, level=0):
    find_toc_nodes, get_toc_node_data = get_functions(level)

    result = []

    for node_element in find_toc_nodes(element):
        node_data = get_toc_node_data(node_element)

        sub_nodes = _parse_toc(node_element, get_functions, level=level + 1)

        node = dict(data=node_data, sub_nodes=sub_nodes)
        result.append(node)

    return result


def _deepcopy_element(element):
    result = copy.deepcopy(element)
    return result


def _filter_element(element, filter):
    for sub_element in element.xpath("./child::node()"):
        if not filter(sub_element):
            element.remove(sub_element)
            continue

        if isinstance(sub_element, l_html.HtmlElement):
            _filter_element(sub_element, filter)


def _str_element(element, with_tail=True):
    result = l_html.tostring(element, with_tail=with_tail).decode()
    return result


def _str_children(element):
    result = "".join(
        _str_element(sub_element, with_tail=False)
        if isinstance(sub_element, l_html.HtmlElement)
        else str(sub_element)
        for sub_element in element.xpath("./child::node()")
    )
    return result


def _class_contains(value):
    result = "contains(concat(' ', normalize-space(@class), ' '), ' {} ')".format(value)
    return result


#


if __name__ == "__main__":
    import argparse

    # parse arguments

    parser = argparse.ArgumentParser()

    parser.add_argument("wslob", help="Path to wiktionary slob file")

    arguments = parser.parse_args()

    #

    with slob.open(arguments.wslob) as wiktionary_slob:
        application.run()
